package by.itra.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    private static final String ERROR = "check you date";

    public static void main(String[] args) {
        BufferedReader reader = getReader();
        try {
            while (true) {
                String operation;
                Double first;
                Double second;
                System.out.println("Choose operation: + or sqrt or pow or exit");
                operation = getOperator(reader);
                if ("+".equals(operation)) {
                    System.out.println("Enter first number");
                    first = getNumber(reader);
                    System.out.println("Enter second number");
                    second = getNumber(reader);
                    System.out.println("Answer = " + Calculator.add(first, second));
                } else if ("sqrt".equals(operation)) {
                    System.out.println("Enter  number");
                    first = getNumber(reader);
                    System.out.println("Answer = " + Calculator.sqrtFromNumber(first));
                } else if ("pow".equals(operation)) {
                    System.out.println("Enter first number");
                    first = getNumber(reader);
                    System.out.println("Enter power");
                    second = getNumber(reader);
                    System.out.println("Answer = " + Calculator.powNumbers(first, second));
                } else if (operation.equals("exit")) {
                    close(reader);
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println("error : " + e.getMessage());
        } finally {
            close(reader);
        }
    }

    private static Double getNumber(BufferedReader reader) {
        Double result = null;
        try {
            String date = reader.readLine();
            result = Double.parseDouble(date);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(ERROR);
        }
        return result;
    }


    private static String getOperator(BufferedReader reader) {
        String result = "";
        try {
            result = reader.readLine();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(ERROR);
        }
        return result;
    }

    public static void close(BufferedReader reader) {
        try {
            reader.close();
        } catch (Exception e) {
            System.out.println("Cannot close stream");
        }
    }

    public static BufferedReader getReader() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(System.in));
        } catch (Exception e) {
            System.out.println("Cannot open stream");
        }
        return reader;
    }

}
