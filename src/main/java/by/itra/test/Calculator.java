package by.itra.test;


public class Calculator {

    public static double add(double first, double second){
        return first + second;
    }

    public static double powNumbers(double first, double second){
        return Math.pow(first,second);
    }

    public static double sqrtFromNumber(double first){
        return Math.sqrt(first);
    }

}
